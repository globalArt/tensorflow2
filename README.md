TensorFlow2

- TF site: https://js.tensorflow.org/
- TF git: https://github.com/tensorflow/tfjs-examples/tree/master/mnist
- TF tutorials: https://js.tensorflow.org/tutorials/
- materijali sa radionice: https://drive.google.com/file/d/1XK3QuVdM7AbW8aBI8DhwjjPloVOfEP22/view
- [Neural Playground](https://playground.tensorflow.org/#activation=relu&batchSize=15&dataset=spiral&regDataset=reg-plane&learningRate=0.03&regularizationRate=0&noise=5&networkShape=8,8,5,3&seed=0.66840&showTestData=false&discretize=false&percTrainData=50&x=true&y=true&xTimesY=true&xSquared=true&ySquared=true&cosX=false&sinX=true&cosY=false&sinY=true&collectStats=false&problem=classification&initZero=false&hideText=false)
