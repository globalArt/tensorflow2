
// Stvaranje modela za predviđanje izlaza
const model = tf.sequential();

model.add(tf.layers.dense({
    units: 1,
    inputShape: 2,
    activation: 'sigmoid'
}));

model.compile({
    loss: 'binaryCrossentropy',
    optimizer: 'sgd',
    lr: 0.1
});

// Stvaranje podataka za uvježbavanje
const ulaz = tf.tensor2d([[0, 0], [0, 1], [1, 0], [1, 1]]);

async function uvjezbajMrezu(iz1, iz2, iz3, iz4) {
    
    // Iz ulaznih parametara radimo novi tenzor
    const izlaz = tf.tensor2d([[iz1], [iz2], [iz3], [iz4]])

    // Uvježbavanje modela
    await model.fit(ulaz, izlaz, {
        batchSize: 1,
        epochs: 300,
        callbacks: {
            onEpochEnd: (epoch, log) => {
                ispis(epoch, log.loss)
            }
        }
    });
}


$(function () {
    $("#uvjezbaj").click(async function () {
        
        $("#info").removeClass("alert-success").addClass("alert-warning");
        
        var iz1 = $("#izlaz1").val();
        var iz2 = $("#izlaz2").val();
        var iz3 = $("#izlaz3").val();
        var iz4 = $("#izlaz4").val();

        await uvjezbajMrezu(iz1, iz2, iz3, iz4);

        $("#info").removeClass("alert-warning").addClass("alert-success");
        $("#info").append("<br/>Uvježbavanje završeno");
    });

    $("#racunaj").click(function () {

        // DODATI DOHVAĆANJE IZLAZA IZ MODELA
        var izlaz = model.predict(ulaz).dataSync();
        
        $("#rez1").html(izlaz[0]);
        $("#rez2").html(izlaz[1]);
        $("#rez3").html(izlaz[2]);
        $("#rez4").html(izlaz[3]);
    });

});

function ispis(epoha, greska) {

    var gr = greska.toFixed(6);
    $("#info").html(`Uvježbavanje u tijeku - epoha: ${epoha} - greška ${gr}.`)
}


